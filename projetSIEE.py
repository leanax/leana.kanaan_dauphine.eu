# TEST
# -*-coding:Utf-8 -*

import os

# L'ordi récupère le nom
# nom = input("Quel est votre nom ? ")

nom = 'Leana'
# puis retourne la valeur
print("L’utilisateur est ", str(nom))

msg = """
   93  git commit -am 'input and upper'
   94  git push
leanakanaan@MacBook-Air-de-Marwan leana.kanaan_dauphine.eu %
"""

# age = 23
# print ("l'age de l'utilisateur est", age, " ans")

print('Programme qui donne le nombre d années restantes avant 30 ans')

# Programme demande age
# si age est < à 30 alors
# le programme retourne : 30-age
# si non, le programme retourne "Vous êtes plus vieux que 30 ans"

age= 44  # int(input("Quel est votre age?"))
print ("l'age de l'utilisateur est", age, " ans")
if age < 30:
    print ('il vous reste', 30-age, ' années avant d avoir 30 ans')
else:
    print ("vous avez déjà dépassé 30 ans")
print('    ')
print('    ')
print('    ')
print('Les listes')
print('    ')

l = ['tulipe', 'rose', 'iris', 'lysanthius', 'ronuncule', 'orchidée', 14]

print('Nous travaillons sur la liste suivante:', l)
print('(1) Retirez l intru')
i=0
for i in range(0, len(l)) :
    if type(l[i]) is int:
        l.remove(l[i])
print('voici la liste sans l intru :', l)
print('    ')
print('Ranger la liste par ordre alphabétique')
l.sort()
print('voici la liste rangée dans l ordre alphabétique:', l)
print('    ')
print('Rechercher un élément dans la liste et l ajouter s il n existe pas')

print('availables flowers: ', l)

# newfleur = input(" quelle fleur voulez vous ajouter au bouquet? ")
newfleur = input(" nouvelle fleur:  ")
print(444444444444444, newfleur, type(newfleur))

if str(newfleur).lower() in l:
    print("vous avez déjà cette fleur!")
else:
    l.append(newfleur)
    print('le/la',newfleur, " a été ajouté au bouquet!")

print('    ')
print('    ')
print('Les chaînes de caractères')
print('    ')
print('(1) Compter le nombre de caractère dans la chaîne de caractère (en retirant les espaces)')
s = 'master 244 siee'

print('comptons le nombre de caractère dans la chaîne master 244 siee, sans les espaces!')
i = 0
for long in range(0, len(s)):
    if s[long] != " ":
        i=i+1
print('il y a', i, 'de lettres')
print('    ')
print('(2) mettre en majuscule et en minuscule et inverser majuscule/minuscule')
print('Essayons de mettre master 244 siee en majuscule!')
print(s.upper())
w = 'WAHOU'
print('Essayons de mettre WAHOU en minuscule!')
print(w.lower())
test2= 'BonJour'
print('inversons mes maj/min du mot BonJour')
print(test2.swapcase())
print('    ')
print('(3) transformer la chaine en liste')
print('transformons la chaine master 244 siee en liste!')
print(s.split())
print('    ')
y= 'bonj-ur'
print('(4) La chaîne de caractère',y,' ne contient aucun caractères spéciaux ?')
print(y.isalnum())
print('    ')
s = 'master 244 siee'
print('(5) La chaîne de caractère',s,' ne contient que des nombres?')
print(s.isdigit())
test='23443453455'
print('la chaîne', test,'ne contient que des nombres?')
print(test.isdigit())


print('    ')
phr = 'salut ! ça va ?'
print('(6) Par quoi se termine la chaîne de caractère',phr)
tr = phr.endswith('?')
print(tr)
fsl = phr.endswith('!')
print(fsl)


print('    ')
print('(7) nombre d occurence d un caractère')
s = 'master 244 siee'
print('la lettre e est répétée',s.count('e'), 'fois')
print('    ')
print('(8) utilisation de la fonction expandtabs')
tab= 'master\t244\tSIEE'
test8= tab.expandtabs()
print(test8)
print('    ')
print('(9)Vérification des caractères décimaux')
print('la chaîne 1234 contient-elle uniquement des caractères décimaux?')
s= '1234'
print(s.isdecimal())
print('la chaîne b0njour contient-elle uniquement des caractères décimaux?')
s='b0njour'
print(s.isdecimal())
print('la chaîne bonjour contient-elle uniquement des caractères décimaux?')
s='bonjour'
print(s.isdecimal())
print(' ')
print( ' (10) utilisation de la fonction center')
txt = "coucou"
print('centrons le mot', txt,'au milieu des 0')
x = txt.center(100, "0")
print(x)
print(' ')
print('(11) Remettre du texte à la ligne')
texte= '''ceci est
mon exemple pour montrer
que je sais remettre du texte
à la ligne. '''
print(texte)
print(' ')
print('(12) récupérer des caractères')
print('(aller chercher un caractère à une position précise')
texte='je decouvre python!'
print('le texte est : je decouvre python!')
x=int(input('quel caractère voulez-vous récupérer? (entre 1 et 19)'))
print('le',x,'eme caractère de la chaine est [oui/non]', texte[x])
print(' ')


texte='je decouvre python!'
question= input('voulez-vous connaîtres tous les caractère de la chaîne?[oui/non]')

if question == 'oui':
    print('faisons apparaitre un par un chaque caractère de la chaine')
    c=0
    for i in range(0, len(texte)):
        c=c+1
        print('le caractère à la', c,'eme position est un',texte[i])
else :
    print('tant pis!')

print(' ')
print(' ')
print('Maintenant on travaille avec les dictionnaires!')
print( '(1) On commence par définir un dictionnaire')
print(' ')
dico = {'nom': 'Kanaan', 'prenom' : 'Léana', 'age': 23, 'taille': 165, 'master': 'SIEE', 'genre': 'fille'}


question = input('voulez-vous voir le dictionnaire? [oui/non]')
if question == 'oui':
      print(dico)
else:
      print('ok! ciao')
      print(' ')

print('(2) Essayons de récupérer un élément dans le dictionnaire')
print(' ')

x = input('quelle information voulez-vous récupérer dans le dico? [nom/prenom/age/taille/master/genre]')
rec= dico.get(x)
print(rec)

print(' ')
print('(3) Affichons tous les éléments du dictionnaire')



q= input('voulez-vous consulter les valeurs du dictionnaire ?[oui/non]')

if q =='oui':
    for v in dico.values():
        print(v)
else :
        print ('ok, ciao')

print(' ')
print('(4) Affichons tous les clefs du dictionnaire')

for c in dico.keys():
    print(c)

print(' ')
print(' un peu de fleur maintenant :) ')
print(' ')

fleur = {'fleur1': 'roses', 'fleur2': 'jacynthes', 'fleur3':'tulipes'}
print ('dans mon bouquet j ai des...')

for c in fleur.values():
    print(c)
couleur = {'couleur': 'rouge', 'couleur2':'blanche', 'couleur3':'orange' }
print(' ')
print('et mes fleurs sont de couleurs...')
for d in couleur.values():
    print(d)

print(' ')
print(' on fait un nouveau bouquet maintenant!')

feuillage = {'f1' :'eucaliptus'}
fleurs ={ 'f2' :'tournesols'}
print('faisons un bouquet ! (on fusionne les dicos)')
    
feuillage.update(fleurs)
print(feuillage)

q=input('voulez-vous ajouter une fleur au bouquet?[oui/non]')
if q=='non':
        print('ok bye')
else :
    q2= input('quelle fleur voulez vous ajouter? [insert flower name]')
    feuillage['f3']= q2
    print (feuillage)
    print('dans le bouquet, il y a maintenant des...')
    for s in feuillage.values():
        print(s)
    print('quel beau bouquet')
        
print(' ')
print(' ')
print(' ')
print(' ')
print('Maintenant on va essayer un truc un peu plus compliqué')
print('On va générer aléatoirement un jolie bouquet de fleurs')
import random
#faire une liste de fleur
print('dans la liste de départ, il y a 10 types de fleurs différentes:')
l =['marguerites','roses', 'tulipes', 'pivoines','tournesoles','eucaliptus','menthes','lysanthius','renoncules','gypsophiles']
print(l)
#on veut générer aléatoire des bouquet
#on demande au client combien de fleur il veut dans son bouquet

q=int(input('combien de fleur souhaitez-vous dans votre bouquet?'))

#on va créer une nouvelle liste, ce sera le bouquet

bouquet=[]
#on fait une boucle pour remplir le bouquet

for i in range(0,q):
    bouquet= bouquet +[l[random.randint(0,9)]]

print('dans votre bouquet il y a des...', bouquet)
              
print(' ')
print(' ')
print('Manioulation d un logiciel de RH')
print(' ')
print(' on définit la base du bureau')
IT = set(['Hasna','Léana','Sune','Soren'])
business = set(['Elise','Ines','Othmane','Ole'])
manager = set(['Hasna', 'Ole'])
chef = set(['Tatyana'])

print('les auditeurs IT sont', IT, 'les auditeurs business sont', business,'la cheffe de l audit est', chef)

Audit_Office = IT.union(business).union(chef)
print(' ')
print('Euronext Internal Audit Office est donc composé de', Audit_Office)
print(' ')
print('Et si on recrutait !')
x=input('Voulez-vous recruter un.e nouvel.lle auditeur ?[oui/non]')
if x=='non':
      print('nous ne recrutons pas')
else:
    n=input('Veuillez entrer le nom de la personne')
    Audit_Office.add(n)
    print('Bienvenue à',n,' dans le bureau d Audit !')
    print('le bureau est maintenant composé de', Audit_Office)

print(' ')    
x=input('Est-ce qu un auditeur va quitter le bureau ?[oui/non] ')
if x=='non':
    print('le bureau ne change pas')
else:
    n=input('Veuillez entrer le nom de la personne ')
    for bureau in [Audit_Office, IT, business]:
        bureau.discard(n)

print('le bureau est maintenant composé de', Audit_Office)
print(' ')
print(' ')
print(' ')


print('Definition d une politique de mot de passe')
print('Essayons un petit programme')
print(' ')

letter = 0
number = 0
majuscule = 0
OKletter = letter >= 8
OKnumber = number >= 1
OKmajuscule = majuscule >= 1
OK = OKletter and OKnumber and OKmajuscule
tentatives = 0

while tentatives < 3:
    tentatives = tentatives + 1
    newmdp=input(" Veuillez entrer un nouveaux mot de passe (Au moins une majuscule, \
                un chiffre et huit caractères) : ")
    for letter in newmdp:
            letter = ord(letter)
            if 65 <= letter <= 90: # si majuscule
                majuscule += 1
                letter += 1
            elif 97 <= letter <= 122: # si minuscule
                    letter += 1
            elif 48 <= letter <= 57: # si entier entre 0 et 9
                        number += 1
                        print ('Le mot de passe a bien été changé!')





 
      


